package pl.edu.pk.WM.BazyDanych.td.repositories;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import pl.edu.pk.WM.BazyDanych.td.entities.BookCategory;

@Repository
public interface BookCategoryRepository extends PagingAndSortingRepository<BookCategory, Long> {
	Optional<BookCategory> findByName(String name);
}
