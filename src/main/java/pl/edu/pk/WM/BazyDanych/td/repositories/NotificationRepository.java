package pl.edu.pk.WM.BazyDanych.td.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import pl.edu.pk.WM.BazyDanych.td.entities.Notification;

@Repository
public interface NotificationRepository extends PagingAndSortingRepository<Notification, Long> {
}
