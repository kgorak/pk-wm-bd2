package pl.edu.pk.WM.BazyDanych.td.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import pl.edu.pk.WM.BazyDanych.td.entities.BookBorrowing;
import pl.edu.pk.WM.BazyDanych.td.entities.User;

@Repository
public interface BookBorrowingRepository extends PagingAndSortingRepository<BookBorrowing, Long> {

	Page<BookBorrowing> findAllByUser(User user, Pageable pageable);
}
