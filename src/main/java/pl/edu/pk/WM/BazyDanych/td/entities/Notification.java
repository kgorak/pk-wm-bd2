package pl.edu.pk.WM.BazyDanych.td.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "POWIADOMIENIE")
@SequenceGenerator(name = "SEQ_POWIADOMIENIE", sequenceName = "SEQ_POWIADOMIENIE")
@Data
@NoArgsConstructor(onConstructor = @__({ @Deprecated }))
@AllArgsConstructor
public class Notification {

	@Id
	@Column(name = "POWk_1_ID", nullable = false, columnDefinition = "NUMBER(7)")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_POWIADOMIENIE")
	Long ID;

	@Column(name = "POW_TYTUL", columnDefinition = "VARCHAR2(40)")
	String title;

	@Column(name = "POW_TEKST", columnDefinition = "VARCHAR2(40)")
	String text;

	@ManyToOne
	@JoinColumn(name = "UZY_ID", nullable = false, foreignKey = @ForeignKey(name = "FK1_POWIADOMIENIE"))
	User user;
}
