package pl.edu.pk.WM.BazyDanych.td.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.edu.pk.WM.BazyDanych.td.entities.User;
import pl.edu.pk.WM.BazyDanych.td.repositories.UserRepository;

@Slf4j
@RequiredArgsConstructor(onConstructor = @__({ @Autowired }))
@Controller
@RestController
@RequestMapping("/authorization")
public class AuthorizationService {

	final UserRepository userRepository;

	@RequestMapping("/test")
	public ResponseEntity<?> test() {
		return ResponseEntity.ok("test");
	}

	@Data
	@AllArgsConstructor
	public static final class ThinUser {
		String role;
		String username;
		String name;
		String surname;

		public static ThinUser of(User user) {
			return new ThinUser(user.getRole(), user.getLogin(), user.getName(), user.getSurname());
		}
	}

	@RequestMapping("/signIn")
	public ResponseEntity<?> signIn(@AuthenticationPrincipal User user) {
		if (user == null) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

		return ResponseEntity.ok(ThinUser.of(user));
	}

	@Data
	@AllArgsConstructor
	public static class SignUpRequest {
		String username;
		String password;
		String name;
		String surname;
	}

	@PostMapping("/signUp")
	public ResponseEntity<?> signUp(@RequestBody SignUpRequest body) {
		var user = new User(null, "READER", body.username, body.password, body.name, body.surname, null, null);
		user = userRepository.save(user);
		log.info("Dodano użytkownika: {}", user);
		return ResponseEntity.ok(ThinUser.of(user));
	}
}
