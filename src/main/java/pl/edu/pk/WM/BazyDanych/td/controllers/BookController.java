package pl.edu.pk.WM.BazyDanych.td.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.edu.pk.WM.BazyDanych.td.entities.Book;
import pl.edu.pk.WM.BazyDanych.td.entities.BookBorrowing;
import pl.edu.pk.WM.BazyDanych.td.entities.BookRequest;
import pl.edu.pk.WM.BazyDanych.td.entities.BookStatus;
import pl.edu.pk.WM.BazyDanych.td.entities.User;
import pl.edu.pk.WM.BazyDanych.td.repositories.BookBorrowingRepository;
import pl.edu.pk.WM.BazyDanych.td.repositories.BookRepository;
import pl.edu.pk.WM.BazyDanych.td.repositories.BookRequestRepository;

@Slf4j
@RequiredArgsConstructor(onConstructor = @__({ @Autowired }))
@RestController
@RequestMapping("/api/books")
public class BookController {

	private final ObjectMapper objectMapper;
	private final BookRepository bookRepository;
	private final BookRequestRepository bookRequestRepository;
	private final BookBorrowingRepository bookBorrowingRepository;

	@GetMapping
	@Transactional
	public ResponseEntity<?> addBook(Pageable pageable) {
		var books = bookRepository.findAll(pageable);

		var page = new PagedResponse<>(books);

		return ResponseEntity.ok(page);
	}

	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<?> deleteBook(@PathVariable("id") Long id) {
		bookRepository.deleteById(id);

		log.debug("Deleted book with ID: {}", id);

		return ResponseEntity.noContent().build();
	}

	@PostMapping
	@Transactional
	public ResponseEntity<?> addBook(@RequestBody Book book, @AuthenticationPrincipal User user) {
		var role = user.getRole();
		var isReader = "READER".equals(role);

		if (isReader) {
			book.setStatus(BookStatus.REQUESTED);
		}

		book.setUser(user);
		book = bookRepository.save(book);

		if (isReader) {
			BookRequest request = new BookRequest(null, "Oczekująca", book, user);
			bookRequestRepository.save(request);
		}

		log.info("Created book: {}", book);

		return ResponseEntity.status(HttpStatus.CREATED).body(book);
	}

	@PatchMapping("/{id}")
	@Transactional
	public ResponseEntity<?> updateBook(@PathVariable Long id, HttpServletRequest request) throws IOException {
		Book book = bookRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
		Book withChanges = objectMapper.readerForUpdating(book).readValue(request.getReader());
		Book updated = bookRepository.save(withChanges);

		log.debug("Updated book: {} -> {}", book, updated);

		return ResponseEntity.ok(updated);
	}

	@PostMapping("/{id}/borrowings")
	@Transactional
	public ResponseEntity<?> borrowBook(@PathVariable Long id, @AuthenticationPrincipal User user) {
		var book = bookRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Not found"));
		if (book.getStatus() != BookStatus.AVAILABLE) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body("Książka nie może zostać wypożyczona");
		}

		book.setStatus(BookStatus.BORROWED);

		BookBorrowing borrowing = new BookBorrowing(null, LocalDate.now(), null, BigDecimal.ZERO, null, user, book);
		borrowing = bookBorrowingRepository.save(borrowing);

		log.debug("Borrowed book: {}", book);

		return ResponseEntity.status(HttpStatus.CREATED).body(borrowing);
	}
}
