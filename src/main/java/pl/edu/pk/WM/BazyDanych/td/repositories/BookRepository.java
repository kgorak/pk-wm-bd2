package pl.edu.pk.WM.BazyDanych.td.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import pl.edu.pk.WM.BazyDanych.td.entities.Book;

@Repository
public interface BookRepository extends PagingAndSortingRepository<Book, Long> {
}
