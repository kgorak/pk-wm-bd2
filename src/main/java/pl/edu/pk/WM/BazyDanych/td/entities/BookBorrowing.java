package pl.edu.pk.WM.BazyDanych.td.entities;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "WYPOZYCZENIE")
@SequenceGenerator(name = "SEQ_WYPOZYCZENIE", sequenceName = "SEQ_WYPOZYCZENIE")
@Data
@NoArgsConstructor(onConstructor = @__({ @Deprecated }))
@AllArgsConstructor
public class BookBorrowing {

	@Id
	@Column(name = "WYPk_1_ID", nullable = false, columnDefinition = "NUMBER(7)")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_WYPOZYCZENIE")
	Long ID;

	@Column(name = "WYP_DATA_WYPOZYCZENIA", nullable = false)
	LocalDate borrowedAt;

	@Column(name = "WYP_DATA_ZWROTU")
	LocalDate returnedAt;

	@Column(name = "WYP_KOSZT", nullable = false, columnDefinition = "NUMBER(7, 2) DEFAULT 0")
	BigDecimal penalty;

	@ManyToMany
	@JoinTable(name = "KARA_WYPOZYCZENIE",
		joinColumns = {
			@JoinColumn(name = "WYPOk_1_ID", columnDefinition = "NUMBER(7)", foreignKey = @ForeignKey(name = "FK2_KARA_WYPOZYCZENIE")),
		},
		inverseJoinColumns = {
			@JoinColumn(name = "KARAk_1_ID", columnDefinition = "NUMBER(7)", foreignKey = @ForeignKey(name = "FK1_KARA_WYPOZYCZENIE")),
		}
	)
	List<BookPenalty> penalties;	

	@ManyToOne(optional = false)
	@JoinColumn(name = "UZY_ID", nullable = false, updatable = false, foreignKey = @ForeignKey(name = "FK1_WYPOZYCZENIE"))
	User user;

	@ManyToOne(optional = false)
	@JoinColumn(name = "KSI_ID", nullable = false, updatable = false, foreignKey = @ForeignKey(name = "FK2_WYPOZYCZENIE"))
	Book book;
}
