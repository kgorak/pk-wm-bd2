package pl.edu.pk.WM.BazyDanych.td.entities;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "KARA", uniqueConstraints = {
	@UniqueConstraint(columnNames = {"KAR_NAZWA"}, name = "UQ1_KARA")
})
@SequenceGenerator(name = "SEQ_KARA", sequenceName = "SEQ_KARA")
@Data
@NoArgsConstructor(onConstructor = @__({ @Deprecated }))
@AllArgsConstructor
public class BookPenalty {

	@Id
	@Column(name = "KARk_1_ID", nullable = false, columnDefinition = "NUMBER(7)")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_KARA")
	Long ID;

	@Column(name = "KAR_NAZWA", nullable = false, columnDefinition = "VARCHAR2(40)")
	String name;

	@Column(name = "KAR_KOSZT", nullable = false, columnDefinition = "NUMBER(7, 2) DEFAULT 0")
	BigDecimal penalty;

	@ManyToMany(mappedBy = "penalties")
	List<BookBorrowing> bookBorrowings;	
}

