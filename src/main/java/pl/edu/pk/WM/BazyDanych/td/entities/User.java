package pl.edu.pk.WM.BazyDanych.td.entities;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "UZYTKOWNIK", uniqueConstraints = {
	@UniqueConstraint(columnNames = {"UZY_LOGIN"}, name = "UQ1_UZYTKOWNIK")
})
@SequenceGenerator(name = "SEQ_UZYTKOWNIK", sequenceName = "SEQ_UZYTKOWNIK")
@Data
@NoArgsConstructor(onConstructor = @__({ @Deprecated }))
@AllArgsConstructor
@ToString(exclude = {"notification", "bookRequests"})
public class User implements UserDetails {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_UZYTKOWNIK")
	@Column(name = "UZYk_1_ID", columnDefinition = "NUMBER(7)")
	Long ID;


	@Column(name = "UZY_ROLA", nullable = false, columnDefinition = "VARCHAR2(40)")
	String role;

	@Column(name = "UZY_LOGIN", nullable = false, columnDefinition = "VARCHAR2(40)")
	String login;

	@Column(name = "UZY_HASLO", nullable = false, columnDefinition = "VARCHAR2(40)")
	String password;

	@Column(name = "UZY_IMIE", columnDefinition = "VARCHAR2(40)")
	String name;

	@Column(name = "UZY_NAZWISKO", columnDefinition = "VARCHAR2(40)")
	String surname;

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "user")
	@JsonIgnore
	List<Notification> notification;

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "user")
	@JsonIgnore
	List<BookRequest> bookRequests;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Collections.singleton(new SimpleGrantedAuthority(role));
	}

	@Override
	public String getUsername() {
		return login;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
