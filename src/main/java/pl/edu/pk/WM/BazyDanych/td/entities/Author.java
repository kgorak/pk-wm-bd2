package pl.edu.pk.WM.BazyDanych.td.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "AUTOR")
@SequenceGenerator(name = "SEQ_AUTOR", sequenceName = "SEQ_AUTOR")
@Data
@NoArgsConstructor(onConstructor = @__({ @Deprecated }))
@AllArgsConstructor
public class Author {

	@Id
	@Column(name = "AUTk_1_ID", nullable = false, columnDefinition = "NUMBER(7)")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_AUTOR")
	Long ID;

	@Column(name = "AUT_IMIE", nullable = false, columnDefinition = "VARCHAR2(40)")
	String name;

	@Column(name = "AUT_NAZWISKO", nullable = false, columnDefinition = "VARCHAR2(40)")
	String surname;

	@ManyToMany(mappedBy = "authors")
	List<Book> books;
}
