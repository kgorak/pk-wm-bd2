package pl.edu.pk.WM.BazyDanych;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BazyDanychApplication {

	// sqlplus ST41K_02_BottomUp/oraFD546fgg@DB1
	public static void main(String[] args) {
		SpringApplication.run(BazyDanychApplication.class, args);
	}

}
