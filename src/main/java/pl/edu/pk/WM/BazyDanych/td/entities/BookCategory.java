package pl.edu.pk.WM.BazyDanych.td.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "KATEGORIA", uniqueConstraints = {
	@UniqueConstraint(columnNames = {"KAT_NAZWA"}, name = "UQ1_KATEGORIA")
})
@SequenceGenerator(name = "SEQ_KATEGORIA", sequenceName = "SEQ_KATEGORIA")
@Data
@NoArgsConstructor(onConstructor = @__({ @Deprecated }))
@AllArgsConstructor
public class BookCategory {

	@Id
	@Column(name = "KATk_1_ID", nullable = false, columnDefinition = "NUMBER(7)")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_KATEGORIA")
	Long ID;

	@Column(name = "KAT_NAZWA", nullable = false, columnDefinition = "VARCHAR2(40)")
	String name;

	@ManyToMany(mappedBy = "categories")
	List<Book> books;
}
