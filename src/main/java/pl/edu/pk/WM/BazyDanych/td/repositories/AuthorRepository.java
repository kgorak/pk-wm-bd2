package pl.edu.pk.WM.BazyDanych.td.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import pl.edu.pk.WM.BazyDanych.td.entities.Author;

@Repository
public interface AuthorRepository extends PagingAndSortingRepository<Author, Long> {
}

