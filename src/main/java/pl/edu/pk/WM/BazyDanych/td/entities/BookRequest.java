package pl.edu.pk.WM.BazyDanych.td.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "PROPOZYCJA", uniqueConstraints = {
	@UniqueConstraint(columnNames = "KSI_ID", name = "UQ1_PROPOZYCJA")
})
@SequenceGenerator(name = "SEQ_PROPOZYCJA", sequenceName = "SEQ_PROPOZYCJA")
@Data
@NoArgsConstructor(onConstructor = @__({ @Deprecated }))
@AllArgsConstructor
public class BookRequest {

	@Id
	@Column(name = "PROk_1_ID", nullable = false, columnDefinition = "NUMBER(7)")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PROPOZYCJA")
	Long ID;

	@Column(name = "PRO_STATUS", nullable = false, columnDefinition = "VARCHAR2(40)")
	String status;

	@OneToOne(optional = false)
	@JoinColumn(name = "KSI_ID", nullable = false, foreignKey = @ForeignKey(name = "FK1_PROPOZYCJA"))
	Book book;

	@ManyToOne(optional = false)
	@JoinColumn(name = "UZY_ID", nullable = false, updatable = false, foreignKey = @ForeignKey(name = "FK2_PROPOZYCJA"))
	User user;
}
