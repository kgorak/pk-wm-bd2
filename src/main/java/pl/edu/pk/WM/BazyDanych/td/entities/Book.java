package pl.edu.pk.WM.BazyDanych.td.entities;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "KSIAZKA")
@SequenceGenerator(name = "SEQ_KSIAZKA", sequenceName = "SEQ_KSIAZKA")
@Data
@NoArgsConstructor(onConstructor = @__({ @Deprecated }))
@AllArgsConstructor
@ToString(exclude = {"user", "categories", "authors", "bookRequest"})
public class Book {

	@Id
	@Column(name = "KSIk_1_ID", nullable = false, columnDefinition = "NUMBER(7)")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_KSIAZKA")
	Long ID;

	@Enumerated(EnumType.STRING)
	@Column(name = "KSI_STATUS", columnDefinition = "VARCHAR2(40)")
	BookStatus status;

	@Column(name = "KSI_TYTUL", columnDefinition = "VARCHAR2(40)")
	String title;

	@Column(name = "KSI_ISBN", columnDefinition = "VARCHAR2(40)")
	String ISBN;

	@Column(name = "KSI_DATA_WYDANIA")
	LocalDate releasedAt;

	@ManyToOne
	@JoinColumn(name = "UZY_ID", nullable = false, columnDefinition = "NUMBER(7)", foreignKey = @ForeignKey(name = "FK1_KSIAZKA"))
	@JsonIgnore
	User user;

	@ManyToMany
	@JoinTable(name = "KATEGORIA_KSIAZKA",
		joinColumns = {
			@JoinColumn(name = "KSIAk_2_ID", columnDefinition = "NUMBER(7)", foreignKey = @ForeignKey(name = "FK2_KATEGORIA_KSIAZKA")),
		},
		inverseJoinColumns = {
			@JoinColumn(name = "KATEk_1_ID", columnDefinition = "NUMBER(7)", foreignKey = @ForeignKey(name = "FK1_KATEGORIA_KSIAZKA")),
		}
	)
	@JsonIgnore
	List<BookCategory> categories;

	@ManyToMany
	@JoinTable(name = "AUTOR_KSIAZKA",
		joinColumns = {
			@JoinColumn(name = "KSIAk_2_ID", columnDefinition = "NUMBER(7)", foreignKey = @ForeignKey(name = "FK2_AUTOR_KSIAZKA")),
		},
		inverseJoinColumns = {
			@JoinColumn(name = "AUTOk_1_ID", columnDefinition = "NUMBER(7)", foreignKey = @ForeignKey(name = "FK1_AUTOR_KSIAZKA")),
		}
	)
	@JsonIgnore
	List<Author> authors;

	@OneToOne(mappedBy = "book")
	@JsonIgnore
	BookRequest bookRequest;
}
