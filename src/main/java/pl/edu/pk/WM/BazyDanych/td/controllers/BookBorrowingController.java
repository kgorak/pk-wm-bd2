package pl.edu.pk.WM.BazyDanych.td.controllers;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import pl.edu.pk.WM.BazyDanych.td.entities.BookBorrowing;
import pl.edu.pk.WM.BazyDanych.td.entities.User;
import pl.edu.pk.WM.BazyDanych.td.repositories.BookBorrowingRepository;

@RequiredArgsConstructor(onConstructor = @__({ @Autowired }))
@RestController
public class BookBorrowingController {

	private final BookBorrowingRepository bookBorrowingRepository;

	@GetMapping("/api/bookBorrowings/private")
	@Transactional
	public ResponseEntity<?> getUserBorrowings(@AuthenticationPrincipal User user, Pageable pageable) {
		var books = bookBorrowingRepository.findAllByUser(user, pageable);

		var dtos = books.map(BookBorrowingDto::of);

		var page = new PagedResponse<>(dtos);

		return ResponseEntity.ok(page);
	}

	@Data
	@AllArgsConstructor
	public static class BookBorrowingDto {
		Long ID;
		String bookTitle;
		LocalDate borrowedAt;
		LocalDate returnedAt;
		BigDecimal penalty;

		public static BookBorrowingDto of(BookBorrowing b) {
			return new BookBorrowingDto(b.getID(), b.getBook().getTitle(), b.getBorrowedAt(), b.getReturnedAt(), b.getPenalty());
		}
	}
}
