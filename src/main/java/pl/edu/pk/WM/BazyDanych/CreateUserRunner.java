package pl.edu.pk.WM.BazyDanych;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.edu.pk.WM.BazyDanych.td.entities.Author;
import pl.edu.pk.WM.BazyDanych.td.entities.Book;
import pl.edu.pk.WM.BazyDanych.td.entities.BookCategory;
import pl.edu.pk.WM.BazyDanych.td.entities.BookRequest;
import pl.edu.pk.WM.BazyDanych.td.entities.BookStatus;
import pl.edu.pk.WM.BazyDanych.td.entities.User;
import pl.edu.pk.WM.BazyDanych.td.repositories.AuthorRepository;
import pl.edu.pk.WM.BazyDanych.td.repositories.BookCategoryRepository;
import pl.edu.pk.WM.BazyDanych.td.repositories.BookRepository;
import pl.edu.pk.WM.BazyDanych.td.repositories.BookRequestRepository;
import pl.edu.pk.WM.BazyDanych.td.repositories.UserRepository;

@Component
@RequiredArgsConstructor(onConstructor = @__({ @Autowired }))
@Slf4j
class CreateUserRunner implements CommandLineRunner {

	private final AuthorRepository authorRepository;
	private final UserRepository userRepository;
	private final BookRepository bookRepository;
	private final BookCategoryRepository bookCategoryRepository;
	private final BookRequestRepository bookRequestRepository;

	@Value("${spring.jpa.hibernate.ddl-auto}")
	String ddl;

	@Override
	public void run(String... args) throws Exception {
		createTestUsers();
	}

	private void createTestUsers() {
		getOrCreateUser("admin", "ADMIN", "password", "Admin", "Adamiński");
		getOrCreateUser("librarian", "LIBRARIAN", "password", "Bibliotekarz", "Bibliotekarski");
		getOrCreateUser("reader", "READER", "password", "Czytelnik", "Czyteliński");
	}

	private User getOrCreateUser(String login, String role, String password, String name, String surname) {
		var user = userRepository.findByLogin(login);
		if (user.isPresent()) {
			return user.get();
		}

		log.info("Creating user {}", login);
		User newUser = new User(null, role, login, password, name, surname, null, null);
		newUser = userRepository.save(newUser);
		return newUser;
	}

	private void createTestData() {
		if (!("create-drop".equals(ddl) || "create".equals(ddl))) {
			log.info("Skipping adding data!");
			return;
		}

		User admin = userRepository.findByLogin("admin")
				.orElseGet(() -> new User(null, "ADMIN", "admin", "password", "Admin", "Adamiński", null, null));
		if (admin.getID() == null) {
			var user = userRepository.save(admin);
			log.info("Added ADMIN: {}", user);
		}

		User librarian = userRepository.findByLogin("librarian").orElseGet(
				() -> new User(null, "LIBRARIAN", "librarian", "password", "Bibliotekarz", "Bibliotekarski", null, null));
		if (librarian.getID() == null) {
			var user = userRepository.save(librarian);
			log.info("Added LIBRARIAN: {}", user);
		}

		User reader = userRepository.findByLogin("reader")
				.orElseGet(() -> new User(null, "READER", "reader", "password", "Czytelnik", "Czyteliński", null, null));
		if (reader.getID() == null) {
			var user = userRepository.save(reader);
			log.info("Added READER: {}", user);
		}

		var categories = createBookCategories();
		var authors = createAuthors(10);

		addBooks(librarian, 100, subset(categories, 0.2), subset(authors, 0.2));
		addBooks(reader, 10, subset(categories, 0.2), subset(authors, 0.2));

		log.info("Created test data");
	}

	private List<Book> addBooks(User user, int count, List<BookCategory> categories, List<Author> authors) {
		var books = new ArrayList<Book>();
		for (int i = 0; i < count; i++) {
			var isbn = StringGenerator.ofLength(5);
			var title = StringGenerator.ofLength(5);
			var b = new Book(null, BookStatus.AVAILABLE, "Title_" + title, "ISBN_" + isbn, LocalDate.now(), user, categories, authors, null);
			b = bookRepository.save(b);
			books.add(b);
		}

		if ("READER".equals(user.getRole())) {
			for (var b : books) {
				var br = new BookRequest(null, "WAITING", b, user);
				br = bookRequestRepository.save(br);
			}
		}
		return books;
	}

	private BookCategory bookCategory(String name) {
		var cat = bookCategoryRepository.findByName(name).orElseGet(() -> new BookCategory(null, name, null));

		if (cat.getID() == null) {
			cat = bookCategoryRepository.save(cat);
		}

		return cat;
	}

	private List<BookCategory> createBookCategories() {
		return Arrays.asList(bookCategory("Angry Birds"), bookCategory("Auta"), bookCategory("Bajki i baśnie"),
				bookCategory("Bajki na dobranoc"), bookCategory("Barbie"), bookCategory("Basia"),
				bookCategory("Bob Budowniczy"), bookCategory("Bolek i Lolek"), bookCategory("CzuCzu"),
				bookCategory("Czytam sobie"), bookCategory("Czytamy bez mamy"), bookCategory("Ćwiczenia grafomotoryczne"),
				bookCategory("Dora poznaje świat"), bookCategory("Dziennik Cwaniaczka"), bookCategory("Edukacja"),
				bookCategory("Foto Słowniki"), bookCategory("Franklin"), bookCategory("Hania Humorek"),
				bookCategory("Karty kolekcjonerskie"), bookCategory("Kolorowanki"), bookCategory("Kraina Lodu"),
				bookCategory("Książeczki edukacyjne"), bookCategory("Kubuś Puchatek"), bookCategory("Kura Adela"),
				bookCategory("Lilla Lou"), bookCategory("Littlest Pet Shop"), bookCategory("Łamigłówki i nauka"),
				bookCategory("Mała dziewczynka"), bookCategory("Mały chłopiec"), bookCategory("Martynka"),
				bookCategory("Marysia"), bookCategory("Mądra Mysz"), bookCategory("Mikołajek"), bookCategory("Muminki"),
				bookCategory("Naklejki"), bookCategory("Obrazki dla maluchów"), bookCategory("Paddington"),
				bookCategory("Pingwiny z Madagaskaru"), bookCategory("Pixi Ja wiem!"), bookCategory("Poczytajki pomagajki"),
				bookCategory("Psi Patrol"), bookCategory("Samoloty"), bookCategory("Strażak Sam"), bookCategory("Szablony"),
				bookCategory("Szlaczki"), bookCategory("Świnka Peppa"), bookCategory("Tablice suchościeralne"),
				bookCategory("Tomek i przyjaciele"), bookCategory("Tupcio Chrupcio"), bookCategory("Uczę się rysować"),
				bookCategory("Wiersze"), bookCategory("Wróżki"), bookCategory("Zabawa"));
	}

	private List<Author> createAuthors(int count) {
		var authors = new ArrayList<Author>();

		for (int i = 0; i < count; i++) {
			String name = StringGenerator.ofLength(20);
			String surname = StringGenerator.ofLength(20);
			var a = new Author(null, name, surname, null);
			a = authorRepository.save(a);
			authors.add(a);
		}

		return authors;
	}

	private <T> List<T> subset(Collection<T> collection, double chance) {
		var out = new ArrayList<T>();
		for (var e : collection) {
			if (Math.random() > chance) {
				out.add(e);
			}
		}
		return out;
	}

	private static final class StringGenerator {
		static final String alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ";
		static final int alphabetLen = alphabet.length();

		private static String ofLength(int length) {
			StringBuilder sb = new StringBuilder(length);

			for (int i = 0; i < length; i++) {
				int index = (int) Math.floor(Math.random() * alphabetLen);
				char c = alphabet.charAt(index);
				sb.append(c);
			}

			return sb.toString();
		}
	}
}
