package pl.edu.pk.WM.BazyDanych.td.controllers;

import java.util.List;

import org.springframework.data.domain.Page;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class PagedResponse<T> {

	private List<T> data;
	private CustomPage page;

	public PagedResponse(Page<T> page) {
		this.data = page.getContent();
		this.page = new CustomPage(page.getNumber(), page.getTotalElements(), page.getSize());
	}

	@Data
	@AllArgsConstructor
	public static class CustomPage {
		public int page;
		public long totalElements;
		public int size;
	}
}
