module.exports = {
	root: true,
	env: {
		node: true,
	},
	extends: ["plugin:vue/recommended", "eslint:recommended"],
	parserOptions: {
		parser: "babel-eslint",
	},
	rules: {
		"no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
		"no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
		"indent": ["warn", "tab"],
		"vue/html-indent": ["warn", "tab"],
		"no-trailing-spaces": "warn",
		"quotes": ["warn", "double"],
		"semi": ["warn", "never"],
		"comma-dangle": ["warn", "always-multiline"],
	},
}
