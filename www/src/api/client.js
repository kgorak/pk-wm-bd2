import axios from "axios"
import store from "@/store"
import router from "@/router"

const client = axios.create({
	baseURL: "http://localhost:8080",
})

const toHeader = (username, password) => {
	const auth = btoa(`${username}:${password}`)
	return `Basic ${auth}`
}

client.interceptors.request.use(config => {
	if (config.url?.startsWith("/authorization")) {
		return config
	}

	const {username, password} = store.state.auth
	if ((!username || username === "")
		|| (!password || password === "")) {
		router.push({name: "SignIn"})
		return Promise.reject("missing credentials")
	}

	config.headers["Authorization"] = toHeader(username, password)

	return config
})

const api = {
	async signIn({username, password}) {
		try {
			const res = await client.post("/authorization/signIn", null, {
				headers: {
					"Authorization": toHeader(username, password),
				},
			})
			const {role} = res.data
			store.dispatch("setCredentials", {username, password, role})
			return res
		} catch (err) {
			if (!err.isAxiosError) {
				throw err
			}

			if (err.response.status === 401) {
				throw "Błędna nazwa użytkownika lub hasło"
			}

			throw err
		}
	},
	async signUp(body) {
		const res = await client.post("/authorization/signUp", body)
		const {username, password} = body
		const { role } = res.data
		store.dispatch("setCredentials", {username, password, role})
		return res
	},
	async getBooks({page = 0, size = 20}) {
		const res = await client.get("/api/books", {
			params: { page, size },
		})
		return res
	},
	async addBook(body) {
		return await client.post("/api/books", body)
	},
	async editBook(id, body) {
		return await client.patch(`/api/books/${id}`, body)
	},
	async deleteBook(id) {
		return await client.delete(`/api/books/${id}`)
	},
	async borrowBook(id) {
		return await client.post(`/api/books/${id}/borrowings`)
	},
	async getUserBorrowings({page, size}) {
		return await client.get("/api/bookBorrowings/private", {
			params: { page, size },
		})
	},
}

export default api
