import "minstyle.io/css/minstyle.io.css"
import "tailwindcss/tailwind.css"

import Vue from "vue"
import App from "./App.vue"
import router from "./router"
import store from "./store"
import api from "./api/client"

Vue.config.productionTip = false

Vue.use({
	install(Vue) {
		Vue.prototype.$api = api
	},
})

new Vue({
	router,
	store,
	render: h => h(App),
}).$mount("#app")
