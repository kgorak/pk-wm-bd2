import Vue from "vue"
import Vuex from "vuex"

Vue.use(Vuex)

export default new Vuex.Store({
	state: {},
	mutations: {},
	actions: {},
	modules: {
		auth: {
			state: () => ({
				username: null,
				password: null,
				role: null,
			}),
			mutations: {
				setCredentials(state, {username, password, role}) {
					state.username = username
					state.password = password
					state.role = role
				},
			},
			actions: {
				setCredentials(context, body) {
					context.commit("setCredentials", body)
				},
			},
		},
	},
})
