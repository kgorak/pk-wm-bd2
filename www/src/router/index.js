import Vue from "vue"
import VueRouter from "vue-router"
import store from "@/store"
import SignUp from "@/views/SignUp.vue"
import SignIn from "@/views/SignIn.vue"

import Dashboard from "@/views/Dashboard.vue"
import DashboardLibrarian from "@/views/DashboardLibrarian.vue"
import DashboardLibrarianBooks from "@/views/DashboardLibrarianBooks.vue"

import DashboardReader from "@/views/DashboardReader.vue"
import DashboardReaderBooks from "@/views/DashboardReaderBooks.vue"
import DashboardReaderBookBorrowings from "@/views/DashboardReaderBookBorrowings.vue"

Vue.use(VueRouter)

const roleGuard = requiredRole => (_to, _from, next) => {
		const { role } = store.state.auth
		if (role !== requiredRole) {
			next({ name: "SignIn" })
		}
		next()
}

const routes = [
	{
		path: "/sign-up",
		name: "SignUp",
		component: SignUp,
	},
	{
		path: "/sign-in",
		name: "SignIn",
		component: SignIn,
	},
	{
		path: "/",
		redirect: { name: "Dashboard" },
	},
	{
		path: "/dashboard",
		name: "Dashboard",
		component: Dashboard,
	},
	{
		path: "/dashboard/librarian",
		name: "DashboardLibrarian",
		component: DashboardLibrarian,
		beforeEnter: roleGuard("LIBRARIAN"),
		children: [
			{ path: "books", name: "DashboardLibrarianBooks", component: DashboardLibrarianBooks },
			{ path: "*", redirect: "books" },
		],
	},
	{
		path: "/dashboard/reader",
		name: "DashboardReader",
		component: DashboardReader,
		beforeEnter: roleGuard("READER"),
		children: [
			{ path: "books", name: "DashboardReaderBooks", component: DashboardReaderBooks },
			{ path: "bookBorrowings", name: "DashboardReaderBookBorrowings", component: DashboardReaderBookBorrowings },
			{ path: "*", redirect: "books" },
		],
	},
]

const router = new VueRouter({
	mode: "history",
	base: process.env.BASE_URL,
	routes,
})

export default router
