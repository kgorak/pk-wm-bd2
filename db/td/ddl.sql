alter table kara drop constraint UQ1_KARA;
alter table kategoria drop constraint UQ1_KATEGORIA;
alter table propozycja drop constraint UQ1_PROPOZYCJA;
alter table uzytkownik drop constraint UQ1_UZYTKOWNIK;

alter table autor_ksiazka drop constraint FK1_AUTOR_KSIAZKA;
alter table autor_ksiazka drop constraint FK2_AUTOR_KSIAZKA;
alter table kara_wypozyczenie drop constraint FK1_KARA_WYPOZYCZENIE;
alter table kara_wypozyczenie drop constraint FK2_KARA_WYPOZYCZENIE;
alter table kategoria_ksiazka drop constraint FK1_KATEGORIA_KSIAZKA;
alter table kategoria_ksiazka drop constraint FK2_KATEGORIA_KSIAZKA;
alter table ksiazka drop constraint FK1_KSIAZKA;
alter table powiadomienie drop constraint FK1_POWIADOMIENIE;
alter table propozycja drop constraint FK1_PROPOZYCJA;
alter table propozycja drop constraint FK2_PROPOZYCJA;
alter table wypozyczenie drop constraint FK2_WYPOZYCZENIE;
alter table wypozyczenie drop constraint FK1_WYPOZYCZENIE;

drop table autor;
drop table autor_ksiazka;
drop table kara;
drop table kara_wypozyczenie;
drop table kategoria;
drop table kategoria_ksiazka;
drop table ksiazka;
drop table powiadomienie;
drop table propozycja;
drop table uzytkownik;
drop table wypozyczenie;

drop sequence seq_autor;
create sequence seq_autor start with 1 increment by 50;
create table autor (
	autk_1_id NUMBER(7) not null,
	aut_imie VARCHAR2(40) not null,
	aut_nazwisko VARCHAR2(40) not null,
	primary key (autk_1_id)
);

drop sequence seq_kara;
create sequence seq_kara start with 1 increment by 50;
create table kara (
	kark_1_id NUMBER(7) not null,
	kar_nazwa VARCHAR2(40) not null,
	kar_koszt NUMBER(7, 2) DEFAULT 0 not null,
	primary key (kark_1_id)
);

drop sequence seq_kategoria;
create sequence seq_kategoria start with 1 increment by 50;
create table kategoria (
	katk_1_id NUMBER(7) not null,
	kat_nazwa VARCHAR2(40) not null,
	primary key (katk_1_id)
);

drop sequence seq_ksiazka;
create sequence seq_ksiazka start with 1 increment by 50;
create table ksiazka (
	ksik_1_id NUMBER(7) not null,
	ksi_isbn VARCHAR2(40),
	ksi_data_wydania date,
	ksi_status VARCHAR2(40),
	ksi_tytul VARCHAR2(40),
	uzy_id NUMBER(7) not null,
	primary key (ksik_1_id)
);

drop sequence seq_powiadomienie;
create sequence seq_powiadomienie start with 1 increment by 50;
create table powiadomienie (
	powk_1_id NUMBER(7) not null,
	pow_tekst VARCHAR2(40),
	pow_tytul VARCHAR2(40),
	uzy_id NUMBER(7) not null,
	primary key (powk_1_id)
);

drop sequence seq_propozycja;
create sequence seq_propozycja start with 1 increment by 50;
create table propozycja (
	prok_1_id NUMBER(7) not null,
	pro_status VARCHAR2(40) not null,
	ksi_id NUMBER(7) not null,
	uzy_id NUMBER(7) not null,
	primary key (prok_1_id)
);

drop sequence seq_uzytkownik;
create sequence seq_uzytkownik start with 1 increment by 50;
create table uzytkownik (
	uzyk_1_id NUMBER(7) not null,
	uzy_login VARCHAR2(40) not null,
	uzy_imie VARCHAR2(40),
	uzy_haslo VARCHAR2(40) not null,
	uzy_rola VARCHAR2(40) not null,
	uzy_nazwisko VARCHAR2(40),
	primary key (uzyk_1_id)
);

drop sequence seq_wypozyczenie;
create sequence seq_wypozyczenie start with 1 increment by 50;
create table wypozyczenie (
	wypk_1_id NUMBER(7) not null,
	wyp_data_wypozyczenia date not null,
	wyp_koszt NUMBER(7, 2) DEFAULT 0 not null,
	wyp_data_zwrotu date,
	ksi_id NUMBER(7) not null,
	uzy_id NUMBER(7) not null,
	primary key (wypk_1_id)
);

create table autor_ksiazka (
	ksiak_2_id NUMBER(7) not null,
	autok_1_id NUMBER(7) not null
);

create table kara_wypozyczenie (
	wypok_1_id NUMBER(7) not null,
	karak_1_id NUMBER(7) not null
);
create table kategoria_ksiazka (
	ksiak_2_id NUMBER(7) not null,
	katek_1_id NUMBER(7) not null
);

alter table kara add constraint UQ1_KARA unique (kar_nazwa);
alter table kategoria add constraint UQ1_KATEGORIA unique (kat_nazwa);
alter table propozycja add constraint UQ1_PROPOZYCJA unique (ksi_id);
alter table uzytkownik add constraint UQ1_UZYTKOWNIK unique (uzy_login);

alter table autor_ksiazka add constraint FK1_AUTOR_KSIAZKA foreign key (autok_1_id) references autor;
alter table autor_ksiazka add constraint FK2_AUTOR_KSIAZKA foreign key (ksiak_2_id) references ksiazka;
alter table kara_wypozyczenie add constraint FK1_KARA_WYPOZYCZENIE foreign key (karak_1_id) references kara;
alter table kara_wypozyczenie add constraint FK2_KARA_WYPOZYCZENIE foreign key (wypok_1_id) references wypozyczenie;
alter table kategoria_ksiazka add constraint FK1_KATEGORIA_KSIAZKA foreign key (katek_1_id) references kategoria;
alter table kategoria_ksiazka add constraint FK2_KATEGORIA_KSIAZKA foreign key (ksiak_2_id) references ksiazka;
alter table ksiazka add constraint FK1_KSIAZKA foreign key (uzy_id) references uzytkownik;
alter table powiadomienie add constraint FK1_POWIADOMIENIE foreign key (uzy_id) references uzytkownik;
alter table propozycja add constraint FK1_PROPOZYCJA foreign key (ksi_id) references ksiazka;
alter table propozycja add constraint FK2_PROPOZYCJA foreign key (uzy_id) references uzytkownik;
alter table wypozyczenie add constraint FK2_WYPOZYCZENIE foreign key (ksi_id) references ksiazka;
alter table wypozyczenie add constraint FK1_WYPOZYCZENIE foreign key (uzy_id) references uzytkownik;
