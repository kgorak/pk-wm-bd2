CLEAR screen;
CLEAR SCREEN;
set linesize 250;
set pagesize 200;
SET SERVEROUTPUT ON;


CREATE or REPLACE PACKAGE random_var
IS
	PRAGMA SERIALLY_REUSABLE;


	---------------------------
	-- RETURN ISBN
	---------------------------
	FUNCTION random_isbn
	RETURN INTEGER;

	---------------------------
	-- RETURN BOOLEAN
	---------------------------
	FUNCTION random_0_1
	RETURN INTEGER;

		---------------------------
	-- RETURN INTEGER
	---------------------------

		FUNCTION random_range(v_min IN INTEGER, v_max IN INTEGER)
	RETURN INTEGER;

		---------------------------
		-- RETURN DATE
		---------------------------

		FUNCTION random_date
	RETURN DATE;

END random_var;
/


CREATE or REPLACE PACKAGE BODY random_var
IS
	PRAGMA SERIALLY_REUSABLE;

	---------------------------
	-- RETURN ISBN
	---------------------------
	FUNCTION random_isbn
	RETURN INTEGER
	IS
	BEGIN
		RETURN floor(dbms_random.value(1000000000000, 9999999999999));
	END;

		---------------------------
	-- RETURN RANDOM BOOLEAN
	---------------------------
	FUNCTION random_0_1
	RETURN INTEGER
	IS
	BEGIN
		RETURN round(dbms_random.value(0,1));
	END;

		---------------------------
	-- RETURN RANDOM RANGE
	---------------------------
	FUNCTION random_range(v_min IN INTEGER, v_max IN INTEGER)
	RETURN INTEGER
	IS
	BEGIN
		RETURN round(dbms_random.value(v_min, v_max));
	END;

		---------------------------
	-- RETURN RANDOM DATE
	---------------------------
	FUNCTION random_date
	RETURN DATE
	IS
	BEGIN
		RETURN  to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd')+dbms_random.value(1,100);
	END;

END random_var;
/


-- RANDOM INTEGER 0,1000
BEGIN
	for var_i in 1 .. 10
	loop
		dbms_output.put_line(random_var.random_isbn());
	end loop;
END;
/
-- RANDOM BOOLEAN
BEGIN
	for var_i in 1 .. 10
	loop
		dbms_output.put_line(random_var.random_0_1());
	end loop;
END;
/
-- RANDOM RANGE
BEGIN
	for var_i in 1 .. 10
	loop
		dbms_output.put_line(random_var.random_range(1, 100));
	end loop;
END;
/
BEGIN
	for var_i in 1 .. 10
	loop
		dbms_output.put_line(random_var.random_date());
	end loop;
END;
/
