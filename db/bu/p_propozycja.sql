CREATE OR REPLACE PACKAGE p_propozycja
is
	procedure propozycja_uzytkownik(tytul in varchar,uzytkownik_id in number,out_pro_id OUT NUMBER);
end;
/

CREATE OR REPLACE PACKAGE BODY p_propozycja
is
	procedure propozycja_uzytkownik(tytul in varchar,uzytkownik_id in number,out_pro_id OUT NUMBER) is
	vksi_id number;
	vpro_id number;
	czy_ksiazka_istnieje number;
	begin
		SELECT count(ksi_tytul) INTO czy_ksiazka_istnieje FROM ksiazka where ksi_tytul=tytul;

		IF czy_ksiazka_istnieje > 0 THEN
			p_ksiazka.dodaj_ksiazke(tytul,null,null,'N',null,null,vksi_id);

			INSERT INTO propozycja (ksi_id,pro_status,uzy_id) VALUES (vksi_id,'N',uzytkownik_id) RETURNING prok_1_id INTO vpro_id;
		ELSE
			DBMS_OUTPUT.PUT_LINE('Taka ksiazka istnieje w bibliotece');
		END IF;

		out_pro_id:=vpro_id;
	END;
END;
/

/*
	declare
		pro_id number;
		begin
		p_propozycja.propozycja_uzytkownik('Biografia legendy',1,pro_id);
		DBMS_OUTPUT.PUT_LINE(pro_id);
	end;
	/

	-- Taka ksiazka istnieje w bibliotece
	declare
		pro_id number;
		begin
		p_propozycja.propozycja_uzytkownik('Biografia legendy',1,pro_id);
		DBMS_OUTPUT.PUT_LINE(pro_id);
	end;
	/
*/
