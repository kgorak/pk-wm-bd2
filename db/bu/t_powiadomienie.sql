CREATE OR REPLACE TRIGGER pow_o_naliczonej_karze
AFTER INSERT ON kara_wypozyczenie
FOR EACH ROW
DECLARE
	id_uzytkownika number;
	id_ksiazki number;
	tytul varchar2(50);
BEGIN
	select uzy_id into id_uzytkownika from wypozyczenie where wypk_1_id=:NEW.wypok_1_id;
	select ksi_id into id_ksiazki from wypozyczenie where wypk_1_id=:NEW.wypok_1_id;
	select ksi_tytul into tytul from ksiazka where ksik_1_id=id_ksiazki;

	INSERT INTO powiadomienie (pow_tytul, pow_tekst, uzy_id) VALUES (tytul, 'Przekroczono termin zwrotu ksiazki', id_uzytkownika);
END;
/
