CREATE OR REPLACE PACKAGE p_ksiazka
is
		procedure dodaj_ksiazke(tytul in varchar,isbn in varchar,data_wydania in date,status in varchar,kategoria_nazwa in varchar,autor_id in char,out_ksi_id OUT NUMBER);
		procedure aktualizuj_ksiazke(ksiazka_id in number,tytul in varchar,isbn in varchar,data_wydania in date,status in varchar, out_ksi_id OUT NUMBER);
		procedure usun_ksiazke(ksiazka_id in number);
		procedure zarezerwuj(ksiazka_nazwa in varchar,out_ksi_id OUT NUMBER);
end;
/

CREATE OR REPLACE PACKAGE BODY p_ksiazka
is
		procedure dodaj_ksiazke(tytul in varchar,isbn in varchar,data_wydania in date,status in varchar,kategoria_nazwa in varchar,autor_id in char,out_ksi_id OUT NUMBER) is
	vksi_id number;
	vkat_id number;
	cnt number;
		check_autor_id NUMBER;
	begin
		INSERT INTO ksiazka (ksi_tytul,ksi_status,ksi_isbn,ksi_data_wydania) VALUES (tytul,status,isbn,data_wydania) RETURNING ksik_1_id INTO vksi_id;

				SELECT COUNT(*) INTO check_autor_id FROM autor where autk_1_id=autor_id;
		IF check_autor_id >= 1 THEN
			INSERT INTO autor_ksiazka (autok_1_id,ksiak_2_id) VALUES (autor_id,vksi_id);
				ELSE
			dbms_output.put_line('Podany autor nie istnieje');
		END IF;

		SELECT count(katk_1_id) INTO cnt FROM kategoria WHERE kat_nazwa=kategoria_nazwa;
		IF kategoria_nazwa IS NOT NULL AND kategoria_nazwa!= ''  THEN
			IF cnt=0 THEN
				INSERT INTO kategoria (kat_nazwa) VALUES (kategoria_nazwa) RETURNING katk_1_id INTO vkat_id;
			ELSE
				SELECT katk_1_id INTO vkat_id FROM kategoria WHERE kat_nazwa=kategoria_nazwa;
			END IF;
		END IF;

		IF vkat_id IS NOT NULL THEN
			INSERT INTO kategoria_ksiazka (katek_1_id,ksiak_2_id) VALUES (vkat_id,vksi_id);
		END IF;
		out_ksi_id:=vksi_id;
	END;

	procedure aktualizuj_ksiazke(ksiazka_id in number,tytul in varchar,isbn in varchar,data_wydania in date,status in varchar, out_ksi_id OUT NUMBER) is
	vksi_id number;
		czy_ksiazka_istnieje number;
	begin
				SELECT count(ksik_1_id) into czy_ksiazka_istnieje from ksiazka;

				IF czy_ksiazka_istnieje=1 THEN
						UPDATE ksiazka SET ksi_tytul=tytul, ksi_isbn=isbn,ksi_data_wydania=data_wydania,ksi_status=status WHERE ksik_1_id=ksiazka_id RETURNING ksik_1_id INTO vksi_id;
				ELSE
						dbms_output.put_line('Ksiazka nie istnieje, wiec nie mozliwa jest jej aktualizacja');
				END IF;

		out_ksi_id:=vksi_id;
	END;

	procedure usun_ksiazke(ksiazka_id in number) is
		czy_ksiazka_istnieje number;
	begin
				IF czy_ksiazka_istnieje=1 THEN
						DELETE FROM autor_ksiazka where ksiak_2_id=ksiazka_id;
						DELETE FROM kategoria_ksiazka where ksiak_2_id=ksiazka_id;
						DELETE FROM ksiazka where ksik_1_id=ksiazka_id;
				ELSE
						dbms_output.put_line('Ksiazka nie istnieje, wiec nie mozliwe jest jej usuniecie');
				END IF;
	END;

	procedure zarezerwuj(ksiazka_nazwa in varchar,out_ksi_id OUT NUMBER) is
	cnt number;
	vksi_id number;
	begin
		SELECT COUNT(*) INTO cnt FROM ksiazka where ksi_tytul=ksiazka_nazwa;
		IF cnt=0 THEN
						dbms_output.put_line('Nie ma wolnych egzemplarzy ksiazki o podanym tytule lub ksiazka nie istnieje');
						out_ksi_id:=-1;
		ELSE
			SELECT KSIK_1_ID INTO vksi_id FROM ksiazka where ksi_tytul=ksiazka_nazwa and ksi_status='O' AND ROWNUM = 1;
			UPDATE ksiazka SET ksi_status='R' where ksik_1_id=vksi_id;
			out_ksi_id:=vksi_id;
		END IF;

	END;
END;
/
/*
		-- Podać id autora który istnieje (1)
		declare
				u_id number;
				begin
				p_ksiazka.dodaj_ksiazke('Harry Potter','12312313213',sysdate-30,'O','Fantasy',3,u_id);
				DBMS_OUTPUT.PUT_LINE(u_id);
		end;
		/

		-- Podany autor nie istnieje
		declare
				u_id number;
				begin
				p_ksiazka.dodaj_ksiazke('Harry Potter','12312313213',sysdate-30,'O','Fantasy',999999999,u_id);
				DBMS_OUTPUT.PUT_LINE(u_id);
		end;
		/

		-- Jako pierwszy argument podać wynik z *(1)
		declare
				u_id number;
				begin
				p_ksiazka.aktualizuj_ksiazke(371,'Harry Potter','12312313213',sysdate-30,'W',u_id);
				DBMS_OUTPUT.PUT_LINE(u_id);
		end;
		/

		-- Ksiazka nie istnieje, wiec nie mozliwa jest jej aktualizacja
		declare
				u_id number;
				begin
				p_ksiazka.aktualizuj_ksiazke(34234571,'Harry Potter','12312313213',sysdate-30,'W',u_id);
				DBMS_OUTPUT.PUT_LINE(u_id);
		end;
		/

		-- Jako argument podać wynik z *(1)
		declare
				begin
				p_ksiazka.usun_ksiazke(371);

		end;
		/

		-- Ksiazka nie istnieje, wiec nie mozliwe jest jej usuniecie
		declare
				begin
				p_ksiazka.usun_ksiazke(37345343251);

		end;
		/

		declare
				u_id number;
				begin
				p_ksiazka.dodaj_ksiazke('Harry Potter 5','12312313213',sysdate-30,'O','Fantasy',3,u_id);
				DBMS_OUTPUT.PUT_LINE(u_id);
		end;
		/
		declare
				u_id number;
				begin
				p_ksiazka.zarezerwuj('Harry Potter 5',u_id);
				DBMS_OUTPUT.PUT_LINE(u_id);
		end;
		/

		-- Nie ma wolnych egzemplarzy ksiazki o podanym tytule lub ksiazka nie istnieje
		declare
				u_id number;
				begin
				p_ksiazka.zarezerwuj('Harry Potter234',u_id);
				DBMS_OUTPUT.PUT_LINE(u_id);
		end;
		/
*/
