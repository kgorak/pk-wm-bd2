CREATE OR REPLACE PACKAGE p_uzytkownik
is
		procedure rejestracja(login in varchar,haslo in varchar,imie in varchar,nazwisko in varchar,rola in char,out_uzy_id OUT NUMBER);
		procedure logowanie(login in varchar,haslo in varchar,out_uzy_id OUT NUMBER);
end;
/

CREATE OR REPLACE PACKAGE BODY p_uzytkownik
is
	procedure rejestracja(login in varchar,haslo in varchar,imie in varchar,nazwisko in varchar,rola in char,out_uzy_id OUT NUMBER) is
	vuzy_id number;
	user_cnt number;
	begin
		SELECT COUNT(*) INTO user_cnt FROM uzytkownik where upper(uzy_login)=upper(login);
		IF user_cnt=0 THEN
			INSERT INTO uzytkownik (uzy_login,uzy_haslo,uzy_imie,uzy_nazwisko,uzy_rola) VALUES (login,haslo,imie,nazwisko,rola) RETURNING uzyk_1_id INTO vuzy_id;
			dbms_output.put_line('Zarejestrowano nowego uzytkownika login='|| login || ' UZY_ID='||vuzy_id);
			out_uzy_id:=vuzy_id;
		ELSE
			dbms_output.put_line('Uzytkownik z tym loginem istnieje login=' || login);
			out_uzy_id:=-1;
		END IF;
	END;

	procedure logowanie(login in varchar,haslo in varchar,out_uzy_id OUT NUMBER) is
	vuzy_id number;
	user_cnt number;
	begin
		SELECT count(uzyk_1_id) INTO user_cnt FROM uzytkownik where uzy_login=login AND uzy_haslo=haslo;
		IF user_cnt!=0 THEN
			SELECT uzyk_1_id INTO vuzy_id FROM uzytkownik where uzy_login=login AND uzy_haslo=haslo;
			out_uzy_id:=vuzy_id;
		ELSE
			dbms_output.put_line('Niepoprawne dane logowania');
			out_uzy_id:=-1;
		END IF;
	END;
END;
/

/*
	declare
		u_id number;
		begin
		p_uzytkownik.rejestracja('log1','haslo','imie','nazwisko','U',u_id);
		DBMS_OUTPUT.PUT_LINE(u_id);
	end;
	/

	declare
		u_id number;
		begin
		p_uzytkownik.logowanie('log1','haslo',u_id);
		DBMS_OUTPUT.PUT_LINE(u_id);
	end;
	/

	-- Uzytkownik z tym loginem istnieje login=log1
	declare
		u_id number;
		begin
		p_uzytkownik.rejestracja('log1','haslo','imie','nazwisko','U',u_id);
		DBMS_OUTPUT.PUT_LINE(u_id);
	end;
	/

	-- Niepoprawne dane logowania
	declare
		u_id number;
		begin
		p_uzytkownik.logowanie('log22','haslo22',u_id);
		DBMS_OUTPUT.PUT_LINE(u_id);
	end;
	/
*/
